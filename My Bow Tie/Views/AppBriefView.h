//
//  AppBriefView.h
//  My Bow Tie
//
//  Created by Alex Motoc on 21/01/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppBriefView : UIView

+ (instancetype)createFromNib;
- (void)populateWithContent;

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
