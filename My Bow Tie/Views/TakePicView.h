//
//  TakePicView.h
//  My Bow Tie
//
//  Created by Alex Motoc on 05/02/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *	A view that displays the "take picture" button. Requires a block for action handling
 */

@interface TakePicView : UIView

@property (nonatomic, copy) void (^buttonActionBlock)(void);
- (void)initializeViewWithButtonActionBlock:(void (^)(void))buttonActionBlock;

@end
