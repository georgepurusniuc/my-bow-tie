//
//  TakePicView.m
//  My Bow Tie
//
//  Created by Alex Motoc on 05/02/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import "TakePicView.h"

@interface TakePicView ()

@end

@implementation TakePicView

- (void)initializeViewWithButtonActionBlock:(void (^)(void))buttonActionBlock {
    self.buttonActionBlock = buttonActionBlock;
    
    CGRect newRect = CGRectMake(8, 8,
                                self.frame.size.height - 16, self.frame.size.height - 16);
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"take_pic_fill"]
            forState:UIControlStateNormal];
    [button setFrame:newRect];
    
    button.adjustsImageWhenHighlighted = YES;
    button.backgroundColor = [UIColor clearColor];
    
    [button addTarget:self
               action:@selector(didTapButton)
     forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:button];
}

- (void)didTapButton {
    if (self.buttonActionBlock) {
        self.buttonActionBlock();
    }
}

// create the border around the view
- (void)drawRect:(CGRect)rect {
    self.backgroundColor = [UIColor clearColor];
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = self.frame.size.height / 2;
    self.layer.borderWidth = 6;
    self.layer.borderColor = [[UIColor whiteColor] CGColor];
}

@end
