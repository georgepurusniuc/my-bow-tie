//
//  AppBriefView.m
//  My Bow Tie
//
//  Created by Alex Motoc on 21/01/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import "AppBriefView.h"
#import "NSString+ContainsString.h"

@interface AppBriefView () <UIWebViewDelegate>

@end

@implementation AppBriefView

+ (instancetype)createFromNib {
    return [[[NSBundle mainBundle] loadNibNamed:@"AppBriefView"
                                          owner:self
                                        options:nil] objectAtIndex:0];
}

// load the html content
- (void)populateWithContent {
    NSBundle *thisBundle = [NSBundle mainBundle];
    NSString *path = [thisBundle pathForResource:@"content" ofType:@"html"];
    
    NSURL *contactURL = [NSURL fileURLWithPath:path];
    [self.webView loadRequest:[NSURLRequest requestWithURL:contactURL]];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType {
    if ([[request.URL absoluteString] containsStringCaseInsensitive:@"mailto"] ||
        [[request.URL absoluteString] containsStringCaseInsensitive:@"content.html"]) {
        return YES;
    }
    
    [[UIApplication sharedApplication] openURL:[request URL]];
    return NO;
}

// close view
- (IBAction)didTapCloseView:(id)sender {
    CGRect newFrame = self.frame;
    newFrame.origin.y += self.superview.frame.size.height;
    
    [UIView animateWithDuration:0.3f animations:^{
        [self setFrame:newFrame];
    } completion:^(BOOL finished) {
        for (UIGestureRecognizer *recognizer in self.superview.gestureRecognizers) {
            [recognizer setEnabled:YES];
        }
        [self removeFromSuperview];
    }];
}

@end
