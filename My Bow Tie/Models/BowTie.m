//
//  BowTie.m
//  My Bow Tie
//
//  Created by Cata Haidau on 06/04/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import "BowTie.h"

@implementation BowTie

+ (instancetype)createWithBowTieId:(NSInteger)bowTieId
                         imageData:(NSData *)imageData
                            gender:(NSString *)gender
                imagePaddingFactor:(CGFloat)paddingFactor {
    BowTie *bowTie = [self new];
    
    bowTie.bowTieId = bowTieId;
    bowTie.imageData = imageData;
    bowTie.gender = gender;
    bowTie.imagePaddingFactor = paddingFactor;
    
    return bowTie;
}

- (UIImage *)image {
    return [UIImage imageWithData:self.imageData];
}

@end
