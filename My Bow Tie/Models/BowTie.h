//
//  BowTie.h
//  My Bow Tie
//
//  Created by Cata Haidau on 06/04/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Realm/Realm.h>

@interface BowTie : RLMObject

@property (nonatomic) NSInteger bowTieId;
@property (strong, nonatomic) NSData *imageData;
@property (strong, nonatomic) NSString *gender;
@property (nonatomic) CGFloat imagePaddingFactor;

+ (instancetype)createWithBowTieId:(NSInteger)bowTieId
                         imageData:(NSData *)imageData
                            gender:(NSString *)gender
                imagePaddingFactor:(CGFloat)paddingFactor;


- (UIImage *)image;

@end
