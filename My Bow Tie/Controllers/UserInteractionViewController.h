//
//  BowTieSelectionViewController.h
//  My Bow Tie
//
//  Created by Alex Motoc on 20/01/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef CGFloat(^MaxThresholdBlock)(void);

@class BowTie;

@protocol BowTieSelectionDelegate <NSObject>

- (void)bowTieSelectionDidSelectBowTie:(BowTie *)bowTie;
- (void)bowTieSelectionDidPanBowTie:(CGFloat)panValue;
- (void)bowTieSelectionDidTapRotateCamera;
- (void)bowTieSelectionDidTapTakePicture;

@end

@interface UserInteractionViewController : UIViewController <UIGestureRecognizerDelegate>

+ (instancetype)createWithMaximumPaddingThresholdBlock:(MaxThresholdBlock)block
                                              delegate:(id <BowTieSelectionDelegate>)delegate;

- (void)handleUserTakenPicture:(UIImage *)picture;

@end
