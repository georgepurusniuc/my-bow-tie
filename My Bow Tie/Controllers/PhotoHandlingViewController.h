//
//  PhotoHandlingViewController.h
//  My Bow Tie
//
//  Created by Alex Motoc on 05/02/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PhotoHandlingDelegate <NSObject>

- (void)photoHandlingDidTapCloseButton;

@end

@interface PhotoHandlingViewController : UIViewController

+ (instancetype)createWithPicture:(UIImage *)image
                         delegate:(id <PhotoHandlingDelegate>)delegate;

@end
