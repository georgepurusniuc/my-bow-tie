//
//  BowTieSelectionViewController.m
//  My Bow Tie
//
//  Created by Alex Motoc on 20/01/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import "UserInteractionViewController.h"
#import "AppBriefView.h"
#import "TakePicView.h"
#import "PhotoHandlingViewController.h"
#import "NSString+ContainsString.h"
#import "BowTie.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <Parse/Parse.h>
#import <Realm/Realm.h>

typedef void (^DownloadBowTiesSuccessBlock)(NSArray *objectsIds);

@interface UserInteractionViewController () <PhotoHandlingDelegate>

@property (weak, nonatomic) id <BowTieSelectionDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *previousButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIView *nextButtonBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *previousButtonBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *infoButtonBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *cameraButtonBackgroundView;
@property (weak, nonatomic) IBOutlet TakePicView *takePicView;
@property (weak, nonatomic) IBOutlet UIView *prevButtonGenderView;
@property (weak, nonatomic) IBOutlet UIView *nextButtonGenderView;
@property (weak, nonatomic) IBOutlet UIButton *setPaddingButton;
@property (weak, nonatomic) IBOutlet UISlider *paddingSlider;
@property (weak, nonatomic) IBOutlet UILabel *paddingLabel;

@property (strong, nonatomic) NSMutableArray *availableBowTies;
@property (strong, nonatomic) AppBriefView *appBriefView;
@property (strong, nonatomic) PhotoHandlingViewController *photoHandlingVC;

@property (copy, nonatomic) MaxThresholdBlock maxThresholdBlock;
@property (nonatomic) NSInteger selectedBowTieIndex;
@property (nonatomic) CGFloat draggingValue;
@property (nonatomic) CGFloat previousDraggingValue;

@end

static NSInteger const kNextButtonTag = 0;
static NSInteger const kPreviousButtonTag = 1;

@implementation UserInteractionViewController

- (instancetype)init {
    self = [super init];
    if (self) {
        self.appBriefView = [AppBriefView createFromNib];
        [self.appBriefView populateWithContent];
        self.availableBowTies = [NSMutableArray array];
    }
    return self;
}

+ (instancetype)createWithMaximumPaddingThresholdBlock:(MaxThresholdBlock)block
                                              delegate:(id<BowTieSelectionDelegate>)delegate {
    UserInteractionViewController *vc = [self new];
    
    vc.selectedBowTieIndex = 0;
    vc.delegate = delegate;
    vc.maxThresholdBlock = block;
    
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initializeButtons];
    
#ifdef DEBUG
    self.setPaddingButton.hidden = NO;
    self.paddingSlider.hidden = NO;
    self.paddingLabel.hidden = NO;
#endif
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    [self populateBowTiesArray];
}

- (void)configureUI {
    if (self.availableBowTies.count) {
        BowTie *bowTie = self.availableBowTies[0];
        self.paddingLabel.text = [NSString stringWithFormat:@"%lf", bowTie.imagePaddingFactor];
        self.paddingSlider.value = bowTie.imagePaddingFactor;
        
        [self.delegate bowTieSelectionDidSelectBowTie:bowTie];
    }
    
    if (self.availableBowTies.count > 1) {
        [self setBowTie:self.availableBowTies[1]
               toButton:self.nextButton];
        
        self.nextButtonBackgroundView.hidden = NO;
    }
}

- (void)sortBowTiesArray {
    self.availableBowTies = [NSMutableArray arrayWithArray:[self.availableBowTies
                                                            sortedArrayUsingComparator:^NSComparisonResult(BowTie *bowTie1, BowTie *bowTie2) {
                                                                return [bowTie1.gender compare:bowTie2.gender] == NSOrderedAscending;
                                                            }]];
} 

- (void)populateBowTiesArray {
    if (self.availableBowTies.count) {
        return;
    }
    
    if (! [[[NSUserDefaults standardUserDefaults] objectForKey:@"shouldDownloadBowTies"] integerValue]) {
        RLMResults *bowTies = [BowTie allObjects];
        
        NSArray *savedIds = [[NSUserDefaults standardUserDefaults] objectForKey:@"bowTiesIds"];
        if (savedIds.count == bowTies.count) {
            self.availableBowTies = [NSMutableArray arrayWithCapacity:bowTies.count];
            for (BowTie *bowTie in bowTies) {
                [self.availableBowTies addObject:bowTie];
            }
            [self sortBowTiesArray];
            [self configureUI];
            return;
        }
    }
    
    [self getThumbnailsBowTiesWithSuccessBlock:^(NSArray *objectsIds){
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [self sortBowTiesArray];
        [self configureUI];
        [self getLargeBowTiesPictures];
    }];
}

- (void)getLargeBowTiesPictures {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm deleteAllObjects];
    [realm commitWriteTransaction];
    
    PFQuery *query = [PFQuery queryWithClassName:@"BowTie"];
    
    [query whereKeyExists:@"objectId"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *quesArray, NSError *error) {
        NSMutableArray *objectsIds = [NSMutableArray array];
        
        for (PFObject *object in quesArray) {
            PFFile *imageFile = [object objectForKey:@"image"];
            NSString *gender = [object objectForKey:@"gender"];
            NSInteger bowTieId = [[object objectForKey:@"bowTieId"] integerValue];
            NSNumber *imagePaddingFactor = [object objectForKey:@"imagePaddingFactor"];
            CGFloat floatPaddingFactor = [imagePaddingFactor floatValue];
            
            // this factor is used to adjust bow tie size (bigger or smaller)
            // it will be multiplied with some other values
            // if the image factor was not set on Parse (is nil), we must provide 1 as default value,
            // because 1 is the neutral element for the "*" operation
            if (!imagePaddingFactor) {
                floatPaddingFactor = 1; // 1 is neutral for the "*" operation
            }
            
            [objectsIds addObject:object.objectId];
            
            [imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                if (! error) {
                    UIImage *image = [UIImage imageWithData:data];
                    BowTie *bowTie = [BowTie createWithBowTieId:bowTieId
                                                      imageData:UIImagePNGRepresentation(image)
                                                         gender:gender
                                             imagePaddingFactor:floatPaddingFactor];
                    
                    [self replaceWithLargeBowTie:bowTie];
                    
                    RLMRealm *realm = [RLMRealm defaultRealm];
                    [realm beginWriteTransaction];
                    [realm addObject:bowTie];
                    [realm commitWriteTransaction];
                }
            }];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@(0) forKey:@"shouldDownloadBowTies"];
        [[NSUserDefaults standardUserDefaults] setObject:objectsIds forKey:@"bowTiesIds"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }];
}

- (void)replaceWithLargeBowTie:(BowTie *)bowTie {
    NSInteger index = 0;
    for (int i = 0; i < self.availableBowTies.count; ++ i) {
        BowTie *bowTie2 = self.availableBowTies[i];
        if (bowTie.bowTieId == bowTie2.bowTieId) {
            index = i;
        }
    }
    [self.availableBowTies replaceObjectAtIndex:index withObject:bowTie];
    
    if (index == self.selectedBowTieIndex) {
        self.paddingLabel.text = [NSString stringWithFormat:@"%lf", bowTie.imagePaddingFactor];
        self.paddingSlider.value = bowTie.imagePaddingFactor;
        [self.delegate bowTieSelectionDidSelectBowTie:bowTie];
    }
}

- (void)getThumbnailsBowTiesWithSuccessBlock:(DownloadBowTiesSuccessBlock)block {
    PFQuery *query = [PFQuery queryWithClassName:@"ThumbnailBowTie"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:NO];
    [query whereKeyExists:@"objectId"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *quesArray, NSError *error) {
        __block NSInteger numberOfBowTies = quesArray.count;
        NSMutableArray *objectsIds = [NSMutableArray array];
        
        if (! numberOfBowTies) { // No bowties
            dispatch_async(dispatch_get_main_queue(), ^{
                block(objectsIds);
            });
        }
        
        for (PFObject *object in quesArray) {
            PFFile *imageFile = [object objectForKey:@"image"];
            NSString *gender = [object objectForKey:@"gender"];
            NSInteger bowTieId = [[object objectForKey:@"bowTieId"] integerValue];
            NSNumber *imagePaddingFactor = [object objectForKey:@"imagePaddingFactor"];
            CGFloat floatPaddingFactor = [imagePaddingFactor floatValue];
            
            // this factor is used to adjust bow tie size (bigger or smaller)
            // it will be multiplied with some other values
            // if the image factor was not set on Parse (is nil), we must provide 1 as default value,
            // because 1 is the neutral element for the "*" operation
            if (!imagePaddingFactor) {
                floatPaddingFactor = 1; // 1 is neutral for the "*" operation
            }
            
            [objectsIds addObject:object.objectId];
            
            [imageFile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                -- numberOfBowTies;
                
                if (! error) {
                    UIImage *image = [UIImage imageWithData:data];
                    BowTie *bowTie = [BowTie createWithBowTieId:bowTieId
                                                      imageData:UIImagePNGRepresentation(image)
                                                         gender:gender
                                             imagePaddingFactor:floatPaddingFactor];
                    [self.availableBowTies addObject:bowTie];
                }
                
                if (! numberOfBowTies) { // Finished downloading bowties (success block)
                    dispatch_async(dispatch_get_main_queue(), ^{
                        block(objectsIds);
                    });
                }
            }];
        }
    }];
}

#pragma mark - initializations

- (void)initializeButtons {
    [self.takePicView initializeViewWithButtonActionBlock:^{
        self.view.hidden = YES;
        [self.delegate bowTieSelectionDidTapTakePicture];
    }];
    
    [self.nextButtonBackgroundView.layer setMasksToBounds:YES];
    [self.nextButtonBackgroundView.layer setCornerRadius:10];
    [self.previousButtonBackgroundView.layer setMasksToBounds:YES];
    [self.previousButtonBackgroundView.layer setCornerRadius:10];
    
    [self.infoButtonBackgroundView.layer setMasksToBounds:YES];
    [self.infoButtonBackgroundView.layer setCornerRadius:self.infoButtonBackgroundView.frame.size.height / 2];
    [self.cameraButtonBackgroundView.layer setMasksToBounds:YES];
    [self.cameraButtonBackgroundView.layer setCornerRadius:self.cameraButtonBackgroundView.frame.size.height / 2];
    
    self.nextButton.tag = kNextButtonTag;
    self.previousButton.tag = kPreviousButtonTag;
    
    self.nextButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.previousButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
}

#pragma mark - DEBUG slider action

- (IBAction)sliderDidUpdate:(UISlider *)sender {
    self.paddingLabel.text = [NSString stringWithFormat:@"%lf", sender.value];
    
    BowTie *bowTie = [self.availableBowTies objectAtIndex:self.selectedBowTieIndex];
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    bowTie.imagePaddingFactor = sender.value;
    [realm commitWriteTransaction];
}

- (IBAction)didTapSetPaddingValue:(id)sender {
    BowTie *bowTie = [self.availableBowTies objectAtIndex:self.selectedBowTieIndex];
    
    __block NSInteger numberOfRequests = 2;
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    [MBProgressHUD showHUDAddedTo:window animated:YES];
    
    PFQuery *bowTieQuery = [PFQuery queryWithClassName:@"BowTie"];
    [bowTieQuery whereKey:@"bowTieId" equalTo:@(bowTie.bowTieId)];
    [bowTieQuery getFirstObjectInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
        if (!error) {
            [object setObject:@(self.paddingSlider.value) forKey:@"imagePaddingFactor"];
            [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                if (succeeded) {
                    --numberOfRequests;
                    
                    if (numberOfRequests == 0) {
                        [MBProgressHUD hideHUDForView:window animated:YES];
                    }
                } else {
                    
                }
            }];
        } else {
        
        }
    }];
    
    PFQuery *thumbQuery = [PFQuery queryWithClassName:@"ThumbnailBowTie"];
    [thumbQuery whereKey:@"bowTieId" equalTo:@(bowTie.bowTieId)];
    [thumbQuery getFirstObjectInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
        if (!error) {
            [object setObject:@(self.paddingSlider.value) forKey:@"imagePaddingFactor"];
            [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                if (succeeded) {
                    --numberOfRequests;
                    
                    if (numberOfRequests == 0) {
                        [MBProgressHUD hideHUDForView:window animated:YES];
                    }
                } else {
                    
                }
            }];
        } else {
            
        }
    }];
}

#pragma mark - user interaction

- (IBAction)didTapPreviousBowTie:(id)sender {
    if (self.selectedBowTieIndex - 1 < 0) {
        return;
    }
    
    --self.selectedBowTieIndex;
    
    if (self.selectedBowTieIndex - 1 < 0) {
        [self setBowTie:nil toButton:self.previousButton];
        self.previousButtonBackgroundView.hidden = YES;
    } else {
        [self setBowTie:self.availableBowTies[self.selectedBowTieIndex - 1] toButton:self.previousButton];
    }
    
    [self setBowTie:self.availableBowTies[self.selectedBowTieIndex + 1] toButton:self.nextButton];
    
    self.nextButtonBackgroundView.hidden = NO;
    
    BowTie *bowTie = [self.availableBowTies objectAtIndex:self.selectedBowTieIndex];
    self.paddingLabel.text = [NSString stringWithFormat:@"%lf", bowTie.imagePaddingFactor];
    self.paddingSlider.value = bowTie.imagePaddingFactor;
    [self.delegate bowTieSelectionDidSelectBowTie:bowTie];
}

- (IBAction)didTapNextBowTie:(id)sender {
    if (self.selectedBowTieIndex + 1 >= self.availableBowTies.count) {
        return;
    }
    
    ++self.selectedBowTieIndex;
    
    if (self.selectedBowTieIndex + 1 >= self.availableBowTies.count) {
        [self setBowTie:nil toButton:self.nextButton];
        self.nextButtonBackgroundView.hidden = YES;
    } else {
        [self setBowTie:self.availableBowTies[self.selectedBowTieIndex + 1] toButton:self.nextButton];
    }
    
    [self setBowTie:self.availableBowTies[self.selectedBowTieIndex - 1] toButton:self.previousButton];
    
    self.previousButtonBackgroundView.hidden = NO;
    
    BowTie *bowTie = [self.availableBowTies objectAtIndex:self.selectedBowTieIndex];
    self.paddingLabel.text = [NSString stringWithFormat:@"%lf", bowTie.imagePaddingFactor];
    self.paddingSlider.value = bowTie.imagePaddingFactor;
    [self.delegate bowTieSelectionDidSelectBowTie:bowTie];
}

- (IBAction)didPan:(UIPanGestureRecognizer *)panGestureRecognizer {
    if ((panGestureRecognizer.state == UIGestureRecognizerStateCancelled
         || panGestureRecognizer.state == UIGestureRecognizerStateEnded)) {
        self.previousDraggingValue = self.draggingValue;
        return;
    } else if (panGestureRecognizer.state == UIGestureRecognizerStateChanged) {
        CGPoint translation = [panGestureRecognizer translationInView:self.view];
        CGFloat value = 0;
        
        UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
        if (orientation == UIDeviceOrientationLandscapeLeft ||
            orientation == UIDeviceOrientationLandscapeRight) {
            value = self.previousDraggingValue - translation.x;
        } else {
            value = self.previousDraggingValue - translation.y;
        }
        
        if (fabs(value) > self.maxThresholdBlock()) {
            return;
        }
        
        self.draggingValue = value;
        [self.delegate bowTieSelectionDidPanBowTie:self.draggingValue];
    }
}

- (IBAction)didSwipeRight:(UISwipeGestureRecognizer *)sender {
    [self didTapPreviousBowTie:nil];
}

- (IBAction)didSwipeLeft:(UISwipeGestureRecognizer *)sender {
    [self didTapNextBowTie:nil];
}

- (IBAction)didTapDetailsButton {
    if ([self.appBriefView isDescendantOfView:self.view]) {
        return;
    }
    
    for (UIGestureRecognizer *recognizer in self.view.gestureRecognizers) {
        [recognizer setEnabled:NO];
    }
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y = self.view.frame.size.height;
    [self.appBriefView setFrame:viewFrame];
    [self.view addSubview:self.appBriefView];
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.appBriefView setFrame:self.view.frame];
    } completion:^(BOOL finished) {
        [self.appBriefView.webView.scrollView flashScrollIndicators];
    }];
}

- (void)handleUserTakenPicture:(UIImage *)picture {
    self.photoHandlingVC = [PhotoHandlingViewController createWithPicture:picture
                                                                 delegate:self];
    
    [self presentViewController:self.photoHandlingVC animated:NO completion:^{
        self.view.hidden = NO;
    }];
}

- (IBAction)didTapCameraButton:(id)sender {
    [self.delegate bowTieSelectionDidTapRotateCamera];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark - PhotoHandlingDelegate

- (void)photoHandlingDidTapCloseButton {
    [MBProgressHUD showHUDAddedTo:self.photoHandlingVC.view animated:NO];
    [self.photoHandlingVC dismissViewControllerAnimated:YES completion:^{
        [MBProgressHUD hideHUDForView:self.photoHandlingVC.view animated:NO];
        self.photoHandlingVC = nil;
    }];
}

#pragma mark - Utils

- (void)setBowTie:(BowTie *)bowTie toButton:(UIButton *)button {
    [button setImage:bowTie.image forState:UIControlStateNormal];
    
    UIView *genderView;
    if (button.tag == kPreviousButtonTag) {
        genderView = self.prevButtonGenderView;
    } else {
        genderView = self.nextButtonGenderView;
    }
    
    if (!bowTie) {
        genderView.hidden = YES;
        return;
    }
    
    if ([bowTie.gender isEqualToString:@"female"]) {
        genderView.backgroundColor = [UIColor redColor];
        genderView.hidden = NO;
    } else if ([bowTie.gender isEqualToString:@"male"]) {
        genderView.backgroundColor = [UIColor blueColor];
        genderView.hidden = NO;
    } else {
        genderView.hidden = YES;
    }
}

@end
