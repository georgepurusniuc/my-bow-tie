//
//  PhotoHandlingViewController.m
//  My Bow Tie
//
//  Created by Alex Motoc on 05/02/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import "PhotoHandlingViewController.h"
#import "UIView+AnimatedSubviews.h"
#import "Utils.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <MessageUI/MessageUI.h>
#import <FacebookSDK/FacebookSDK.h>

@interface PhotoHandlingViewController () <UIActionSheetDelegate, MFMailComposeViewControllerDelegate, UIScrollViewDelegate>

@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) UIImageView *imageView;
@property (weak, nonatomic) id <PhotoHandlingDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic) BOOL didConfigureScrollView;

@end

static NSInteger const kSaveDeniedAlertTag = 3491832;

@implementation PhotoHandlingViewController

+ (instancetype)createWithPicture:(UIImage *)image
                         delegate:(id<PhotoHandlingDelegate>)delegate {
    PhotoHandlingViewController *vc = [self new];
    
    vc.image = image;
    vc.delegate = delegate;
    
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // add the imageView to the scroll view to enable zoom
    
    self.imageView = [[UIImageView alloc] initWithImage:self.image];
    self.imageView.frame = CGRectMake(0, 0, self.image.size.width, self.image.size.height);
    [self.scrollView addSubview:self.imageView];
    
    [self initializeFacebook];
    [self initializeButtons];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (self.didConfigureScrollView) {
        return;
    }
    
    // configure zoom
    
    [self initializeScrollView];
    [self centerScrollViewContents];
    self.didConfigureScrollView = YES;
}

#pragma mark - initializations

- (void)initializeButtons {
    [self.closeButton.layer setMasksToBounds:YES];
    [self.closeButton.layer setCornerRadius:10];
    
    [self.saveButton.layer setMasksToBounds:YES];
    [self.saveButton.layer setCornerRadius:10];
    [self.saveButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
    
    [self.shareButton.layer setMasksToBounds:YES];
    [self.shareButton.layer setCornerRadius:10];
}

- (void)initializeScrollView {
    CGFloat scaleWidth = self.scrollView.frame.size.width / self.imageView.frame.size.width;
    CGFloat scaleHeight = self.scrollView.frame.size.height / self.imageView.frame.size.height;
    CGFloat minScale = MIN(scaleWidth, scaleHeight); // minimum scale based on width and height
    
    self.scrollView.minimumZoomScale = minScale;
    
    self.scrollView.maximumZoomScale = 1.0f; // the actual size of the image
    self.scrollView.zoomScale = minScale; // scale to fit image
    [self.scrollView setContentSize:self.imageView.frame.size];
}

- (void)initializeFacebook {
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded || FBSession.activeSession.isOpen) {
        FBSession *session = [FBSession activeSession];
        [session closeAndClearTokenInformation];
        [session close];
        [FBSession setActiveSession:nil];
        
        // clear facebook cookies
        NSHTTPCookieStorage *cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        NSArray *facebookCookies = [cookies cookiesForURL:[NSURL URLWithString:@"https://facebook.com/"]];
        
        for (NSHTTPCookie *cookie in facebookCookies) {
            [cookies deleteCookie:cookie];
        }
    }
}

#pragma mark - zoom logic

- (void)centerScrollViewContents {
    CGSize boundsSize = self.scrollView.bounds.size;
    CGRect contentsFrame = self.imageView.frame;
    
    if (contentsFrame.size.width < boundsSize.width) {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    } else {
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    } else {
        contentsFrame.origin.y = 0.0f;
    }
    
    [self.imageView setFrame:contentsFrame];
}

#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    [self centerScrollViewContents];
}

#pragma mark - user interaction

- (IBAction)didDoubleTapScrollView:(UITapGestureRecognizer *)recognizer {
    if (self.scrollView.zoomScale != self.scrollView.minimumZoomScale) { // zoom out if zoomed
        [UIView animateWithDuration:0.3f animations:^{
            self.scrollView.zoomScale = self.scrollView.minimumZoomScale;
        }];
        
        return;
    }
    
    // zoom in on double tap, if not zoomed
    
    CGPoint pointInView = [recognizer locationInView:self.imageView];
    
    CGFloat newZoomScale = self.scrollView.maximumZoomScale;
    
    CGSize scrollViewSize = self.scrollView.bounds.size;
    CGFloat w = scrollViewSize.width / newZoomScale;
    CGFloat h = scrollViewSize.height / newZoomScale;
    CGFloat x = pointInView.x - (w / 2.0f);
    CGFloat y = pointInView.y - (h / 2.0f);
    
    CGRect rectToZoomTo = CGRectMake(x, y, w, h);
    [self.scrollView zoomToRect:rectToZoomTo animated:YES];
}

- (IBAction)didTapClose:(id)sender {
    [self.delegate photoHandlingDidTapCloseButton];
}

- (IBAction)didTapSave:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    UIImageWriteToSavedPhotosAlbum(self.image, self,
                                   @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

- (IBAction)didTapShare:(id)sender {
    UIActionSheet *sheet = nil;
        
    if ([FBDialogs canPresentShareDialogWithPhotos]) {
        sheet = [[UIActionSheet alloc] initWithTitle:@"Share"
                                            delegate:self
                                   cancelButtonTitle:@"Cancel"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@"via Mail", @"via Facebook", nil];
    } else {
        sheet = [[UIActionSheet alloc] initWithTitle:@"Share"
                                            delegate:self
                                   cancelButtonTitle:@"Cancel"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@"via Mail", nil];
    }
    
    [sheet showInView:self.view];
}

#pragma mark - Image Save

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(NSDictionary *)info {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (error) {
        if (error.code == -3310) {
            [self imageSaveDenied];
        } else {
            NSString *errorMsg = @"There was an error while attempting to save the image";
            [Utils showErrorWithMessage:errorMsg];
        }
        
        return;
    }
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.saveButton setImage:[UIImage imageNamed:@"done_icon"]
                         forState:UIControlStateNormal];
    } completion:^(BOOL finished) {
        [self.saveButton setEnabled:NO];
    }];
}

- (void)imageSaveDenied {
    NSString *alertText;
    NSString *alertButton;
    
    BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
    if (canOpenSettings) {
        alertText = @"It looks like your privacy settings are preventing us from accessing your Photo Album to save this image. You can fix this by doing the following:\n\n1. Touch the Go button below to open the Settings app.\n\n2. Turn the Photos on.\n\n3. Open this app and try again.";
        
        alertButton = @"Go";
    } else {
        alertText = @"It looks like your privacy settings are preventing us from accessing your Photo Album to save this image. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select this app in the list.\n\n4. Turn the Photos on.\n\n6. Open this app and try again.";
        
        alertButton = @"OK";
    }
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Error"
                          message:alertText
                          delegate:self
                          cancelButtonTitle:alertButton
                          otherButtonTitles:nil];
    alert.tag = kSaveDeniedAlertTag;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (alertView.tag == kSaveDeniedAlertTag) {
        BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
        if (canOpenSettings) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
    }
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) { //share via Mail
        if (![MFMailComposeViewController canSendMail]) {
            [Utils showErrorWithMessage:@"Mail app not configured on this device. Please configure it and try again"];
            return;
        }
        
        [MBProgressHUD showHUDAddedTo:self.view animated:NO];
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        
        mc.mailComposeDelegate = self;
        [mc addAttachmentData:UIImagePNGRepresentation(self.image)
                     mimeType:@"image/png"
                     fileName:@"pic.png"];
        
        [self presentViewController:mc animated:YES completion:^{
            [MBProgressHUD hideHUDForView:self.view animated:NO];
        }];
    } else if (buttonIndex == 1 && [FBDialogs canPresentShareDialogWithPhotos]) { //share via Facebook
        [self shareWithFacebook];
    }
}

#pragma mark - MFMailComposerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error {
    // Close the Mail Interface
    
    [MBProgressHUD showHUDAddedTo:controller.view animated:NO];
    [controller dismissViewControllerAnimated:YES completion:^{
        [MBProgressHUD hideHUDForView:controller.view animated:NO];
    }];
}

#pragma mark - Facebook Share

- (void)shareWithFacebook {
    // If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithPhotos]) {
        FBPhotoParams *params = [[FBPhotoParams alloc] init];
        
        params.photos = @[self.image];
        
        [FBDialogs presentShareDialogWithPhotoParams:params
                                         clientState:nil
                                             handler:^(FBAppCall *call,
                                                       NSDictionary *results,
                                                       NSError *error) {
                                                 if (error) {
                                                     [Utils showErrorWithMessage:error.description];
                                                 }
                                             }];
    } else {         // The user doesn't have the Facebook for iOS app installed.
        [Utils showErrorWithMessage:@"You must install the Facebook app to share a picture"];
    }
}

@end
