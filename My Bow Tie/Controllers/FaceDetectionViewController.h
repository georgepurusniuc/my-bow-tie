//
//  ViewController.h
//  My Bow Tie
//
//  Created by Alex Motoc on 19/01/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserInteractionViewController.h"

@protocol FaceDetectionDelegate <NSObject>

- (void)faceDetectionTookPicture:(UIImage *)picture;

@end

@interface FaceDetectionViewController : UIViewController <BowTieSelectionDelegate>

+ (instancetype)createWithDelegate:(id <FaceDetectionDelegate>)delegate;
- (CGFloat)getMaxPaddingThreshold;

@end

