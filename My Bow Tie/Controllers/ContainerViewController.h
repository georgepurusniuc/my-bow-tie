//
//  ContainerViewController.h
//  My Bow Tie
//
//  Created by Alex Motoc on 20/01/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *	View Controller used to present the camera feed view controller, and on top of that, the user interaction view controller
 */
@interface ContainerViewController : UIViewController

@end
