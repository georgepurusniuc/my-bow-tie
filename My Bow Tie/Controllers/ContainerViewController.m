//
//  MainFatherViewController.m
//  My Bow Tie
//
//  Created by Alex Motoc on 20/01/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import "ContainerViewController.h"
#import "FaceDetectionViewController.h"
#import "UserInteractionViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface ContainerViewController () <FaceDetectionDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) NSArray *bowTiesArray;
@property (strong, nonatomic) FaceDetectionViewController *faceDetectVC;
@property (strong, nonatomic) UserInteractionViewController *userInteractionVC;
@property (weak, nonatomic) IBOutlet UITextView *noAccessTextView;

@end

static NSInteger const kSaveDeniedAlertTag = 3491832;

@implementation ContainerViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self askForCameraPermission];
}

- (void)askForCameraPermission {
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus == AVAuthorizationStatusAuthorized) {
        [self initializeChildViewControllers];
    } else if (authStatus == AVAuthorizationStatusDenied) {
        [self camDenied];
    } else if (authStatus == AVAuthorizationStatusNotDetermined) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo
                                 completionHandler:^(BOOL granted) {
                                     if (granted) {
                                         NSLog(@"Granted access to %@", AVMediaTypeVideo);
                                         [self performSelectorOnMainThread:@selector(initializeChildViewControllers)
                                                                withObject:nil
                                                             waitUntilDone:YES];
                                     } else {
                                         NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                                         [self performSelectorOnMainThread:@selector(camDenied)
                                                                withObject:nil
                                                             waitUntilDone:YES];
                                     }
                                 }];
    } else {
        // impossible, unknown authorization status
        [self camDenied];
    }
}

#pragma mark - camera usage denied handling

- (void)camDenied {
    NSString *alertText;
    NSString *alertButton;
    
    BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
    if (canOpenSettings) {
        alertText = @"It looks like your privacy settings are preventing us from accessing your camera to do face detection. You can fix this by doing the following:\n\n1. Touch the Go button below to open the Settings app.\n\n2. Turn the Camera on.\n\n3. Open this app and try again.";
        
        alertButton = @"Go";
    } else {
        alertText = @"It looks like your privacy settings are preventing us from accessing your camera to do face detection. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select this app in the list.\n\n4. Turn the Camera on.\n\n6. Open this app and try again.";
        
        alertButton = @"OK";
    }
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Error"
                          message:alertText
                          delegate:self
                          cancelButtonTitle:alertButton
                          otherButtonTitles:nil];
    alert.tag = kSaveDeniedAlertTag;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (alertView.tag == kSaveDeniedAlertTag) {
        BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
        if (canOpenSettings) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
        
        self.noAccessTextView.hidden = NO;
    }
}

#pragma mark - initializations

- (void)initializeChildViewControllers {
    if (!self.faceDetectVC) {
        self.faceDetectVC = [FaceDetectionViewController createWithDelegate:self];
        [self addChildViewController:self.faceDetectVC];
        [self.faceDetectVC.view setFrame:self.view.bounds];
        [self.view addSubview:self.faceDetectVC.view];
        [self.faceDetectVC didMoveToParentViewController:self];
    }
    
    if (!self.userInteractionVC) {
        self.userInteractionVC = [UserInteractionViewController createWithMaximumPaddingThresholdBlock:^CGFloat{
            return [self.faceDetectVC getMaxPaddingThreshold];
        }
                                                                                              delegate:self.faceDetectVC];
        
        [self addChildViewController:self.userInteractionVC];
        [self.userInteractionVC.view setFrame:self.view.bounds];
        [self.view addSubview:self.userInteractionVC.view];
        [self.userInteractionVC didMoveToParentViewController:self];
    }
}

#pragma mark - FaceDetectionDelegate

- (void)faceDetectionTookPicture:(UIImage *)picture {
    [self.userInteractionVC handleUserTakenPicture:picture];
}

@end
