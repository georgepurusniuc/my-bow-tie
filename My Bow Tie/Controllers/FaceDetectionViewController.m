//
//  ViewController.m
//  My Bow Tie
//
//  Created by Alex Motoc on 19/01/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import <GPUImage/GPUImageView.h>
#import <GPUImage/GPUImageGammaFilter.h>
#import "FaceDetectionViewController.h"
#import "X2DetectionCamera.h"
#import "Utils.h"
#import "UIImage+Utils.h"
#import "BowTie.h"
#import <Crashlytics/Crashlytics.h>
#import <MBProgressHUD/MBProgressHUD.h>

// you can adjust the face with ±50% of face height
static CGFloat const kDragThresholdPercentage = 0.5f;

@interface FaceDetectionViewController ()

@property (weak, nonatomic) IBOutlet GPUImageView *cameraView;
@property (weak, nonatomic) id <FaceDetectionDelegate> delegate;

@property (strong, nonatomic) X2DetectionCamera *detector;
@property (strong, nonatomic) GPUImageGammaFilter *detectorFilter;
@property (strong, nonatomic) NSArray *usedBowTieImageViews;

@property (strong, nonatomic) UIImage *currentBowTieImage;
@property (strong, nonatomic) BowTie *currentBowTie;

@property (assign, nonatomic) CGAffineTransform cameraOutputToPreviewFrameTransform;
@property (assign, nonatomic) CGAffineTransform portraitRotationTransform;
@property (assign, nonatomic) CGAffineTransform texelToPixelTransform;

@property (nonatomic) CGFloat draggingPercentage;
@property (nonatomic) CGFloat currentMaxThreshold; //changed depending on face height
@property (nonatomic) CGFloat currentFaceHeight;
@property (nonatomic) NSInteger usedBowTiesCount;
@property (nonatomic) NSInteger currentUsedBowTieImageViewIndex;
@property (nonatomic) BOOL bowTiesArePresent;
@property (nonatomic) BOOL didLoadCameraUtils;
@property (nonatomic) BOOL didInitializeTransformations;

@end

@implementation FaceDetectionViewController

+ (instancetype)createWithDelegate:(id<FaceDetectionDelegate>)delegate {
    FaceDetectionViewController *vc = [self new];
    
    vc.delegate = delegate;
    
    return vc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // initialize camera utils only once
    if (!self.didLoadCameraUtils) {
        self.didLoadCameraUtils = YES;
        [self initializeCamera];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // initialize camera output transformations only once
    if (!self.didInitializeTransformations) {
        self.didInitializeTransformations = YES;
        [self initializeTransformations];
    }
}

#pragma mark - camera initializations

- (void)initializeCamera {
    self.detector = [[X2DetectionCamera alloc] init];
    self.detector.outputImageOrientation = UIInterfaceOrientationPortrait;
    self.detector.horizontallyMirrorFrontFacingCamera = YES;
    [self.detector rotateCamera];
    
    self.detectorFilter = [[GPUImageGammaFilter alloc] init];
    [self.detector addTarget:self.detectorFilter];
    
    self.cameraView.fillMode = kGPUImageFillModePreserveAspectRatioAndFill;
    self.cameraView.backgroundColor = [UIColor blackColor];
    [self.detectorFilter addTarget:self.cameraView];
    
    [self.detector beginDetecting:kFaceMetaData
                        codeTypes:nil
               withDetectionBlock:^(SMKDetectionOptions detectionType,
                                    NSArray *detectedObjects,
                                    CGRect clapOrRectZero) {
                   if (detectionType & kFaceMetaData) {
                       [self updateFaceMetadataTrackingViewWithObjects:detectedObjects];
                   }
               }];
    
    [self.detector startCameraCapture];
}

- (void)initializeTransformations {
    NSInteger outputHeight = [[self.detector.captureSession.outputs[0] videoSettings][@"Height"] integerValue];
    NSInteger outputWidth = [[self.detector.captureSession.outputs[0] videoSettings][@"Width"] integerValue];
    
    if (UIInterfaceOrientationIsPortrait(self.detector.outputImageOrientation)) {
        // Portrait mode, swap width & height
        NSInteger temp = outputWidth;
        outputWidth = outputHeight;
        outputHeight = temp;
    }
    
    // Use self.view because self.cameraView is not resized at this point (if 3.5" device)
    CGFloat viewHeight = self.view.frame.size.height;
    CGFloat viewWidth = self.view.frame.size.width;
    
    // Calculate the scale and offset of the view vs the camera output
    // This depends on the fillmode of the GPUImageView
    CGFloat scale;
    CGAffineTransform frameTransform;
    switch (self.cameraView.fillMode) {
        case kGPUImageFillModePreserveAspectRatio:
            scale = MIN(viewWidth / outputWidth, viewHeight / outputHeight);
            frameTransform = CGAffineTransformMakeScale(scale, scale);
            frameTransform = CGAffineTransformTranslate(frameTransform, -(outputWidth * scale - viewWidth) / 2,
                                                        -(outputHeight * scale - viewHeight) / 2);
            break;
        case kGPUImageFillModePreserveAspectRatioAndFill:
            scale = MAX(viewWidth / outputWidth, viewHeight / outputHeight);
            frameTransform = CGAffineTransformMakeScale(scale, scale);
            frameTransform = CGAffineTransformTranslate(frameTransform, -(outputWidth * scale - viewWidth) / 2,
                                                        -(outputHeight * scale - viewHeight) / 2 );
            break;
        case kGPUImageFillModeStretch:
            frameTransform = CGAffineTransformMakeScale(viewWidth / outputWidth, viewHeight / outputHeight);
            break;
    }
    
    self.cameraOutputToPreviewFrameTransform = frameTransform;
    
    // In portrait mode, need to swap x & y coordinates of the returned boxes
    if (UIInterfaceOrientationIsPortrait(self.detector.outputImageOrientation)) {
        // Interchange x & y
        self.portraitRotationTransform = CGAffineTransformMake(0, 1, 1, 0, 0, 0);
    } else {
        self.portraitRotationTransform = CGAffineTransformIdentity;
    }
    
    // AVMetaDataOutput works in texels (relative to the image size)
    // We need to transform this to pixels through simple scaling
    self.texelToPixelTransform = CGAffineTransformMakeScale(outputWidth, outputHeight);
}

#pragma mark - detection actions

- (void)hideAllBowTies {
    self.bowTiesArePresent = NO;
    
    for (UIView *bowTieView in [self.cameraView subviews]) {
        if ([bowTieView.layer.name isEqualToString:@"FaceLayer"]) {
            UIImageView *imageView = (UIImageView *)bowTieView;
            imageView.hidden = YES;
        }
    }
}

// remove bow ties of faces that are no longer visible
- (void)removeExtraBowTies:(NSInteger)extraBowTies {
    for (int i = 0; i < extraBowTies; ++i) {
        UIImageView *currentLayer = [self.usedBowTieImageViews objectAtIndex:i];
        [currentLayer removeFromSuperview];
    }
    
    self.usedBowTieImageViews = [NSArray arrayWithArray:[self.cameraView subviews]];
    self.usedBowTiesCount = [self.usedBowTieImageViews count];
}

// reuse imageView for bowtie image
- (UIImageView *)getReusableBowTieImageView {
    while (self.currentUsedBowTieImageViewIndex < self.usedBowTiesCount) {
        UIImageView *currentImageView = [self.usedBowTieImageViews objectAtIndex:self.currentUsedBowTieImageViewIndex++];
        
        if ([[currentImageView.layer name] isEqualToString:@"FaceLayer"]) {
            [currentImageView setHidden:NO];
            return currentImageView;
        }
    }
    
    return nil;
}

// create new bowtie imageView
- (UIImageView *)getNewBowTieImageView {
    UIImageView *bowTieImageView = [[UIImageView alloc] initWithImage:self.currentBowTieImage];
    bowTieImageView.contentMode = UIViewContentModeScaleAspectFit;
    [bowTieImageView.layer setName:@"FaceLayer"];
    bowTieImageView.hidden = YES;
    
    return bowTieImageView;
}

- (CGRect)modifyFaceRectToSceenBounds:(CGRect)face {
    // Flip the Y coordinate to compensate for coordinate difference
    if (([self.detector cameraPosition] == AVCaptureDevicePositionBack)) {
        face.origin.y = 1.0 - face.origin.y - face.size.height;
    }
    
    // Transform to go from texels, which are relative to the image size to pixel values
    face = CGRectApplyAffineTransform(face, self.portraitRotationTransform);
    face = CGRectApplyAffineTransform(face, self.texelToPixelTransform);
    face = CGRectApplyAffineTransform(face, self.cameraOutputToPreviewFrameTransform);
    
    CGFloat insetVal = 0.085f * face.size.height * self.currentBowTie.imagePaddingFactor;
    
    UIEdgeInsets insets = UIEdgeInsetsMake(insetVal, insetVal, insetVal, insetVal);
    face = UIEdgeInsetsInsetRect(face, insets);
    
    return face;
}

- (CGRect)modifyFaceRectToSceenBoundsWithoutInsets:(CGRect)face {
    // Flip the Y coordinate to compensate for coordinate difference
    if (([self.detector cameraPosition] == AVCaptureDevicePositionBack)) {
        face.origin.y = 1.0 - face.origin.y - face.size.height;
    }
    
    // Transform to go from texels, which are relative to the image size to pixel values
    face = CGRectApplyAffineTransform(face, self.portraitRotationTransform);
    face = CGRectApplyAffineTransform(face, self.texelToPixelTransform);
    face = CGRectApplyAffineTransform(face, self.cameraOutputToPreviewFrameTransform);
    
    self.currentMaxThreshold = kDragThresholdPercentage * face.size.height;
    self.currentFaceHeight = face.size.height;
    
    return face;
}

- (void)resetBowTieDetectionValues {
    self.bowTiesArePresent = YES;
    self.usedBowTieImageViews = [NSArray arrayWithArray:[self.cameraView subviews]];
    self.usedBowTiesCount = [self.usedBowTieImageViews count];
    self.currentUsedBowTieImageViewIndex = 0;
}

- (void)updateFaceMetadataTrackingViewWithObjects:(NSArray *)objects {
    if (objects.count == 0) {
        [self hideAllBowTies];
    } else {
        [self resetBowTieDetectionValues];
        
        if (self.usedBowTiesCount > objects.count) {
            [self removeExtraBowTies:self.usedBowTiesCount - objects.count];
        }
        
        for (AVMetadataFaceObject *metadataObject in objects) {
            UIImageView *bowTieImageView = nil;
            
            // re-use an existing layer if possible
            bowTieImageView = [self getReusableBowTieImageView];
            
            // create a new one if necessary
            if (!bowTieImageView) {
                bowTieImageView = [self getNewBowTieImageView];
                [self.cameraView addSubview:bowTieImageView];
            }
            
            CGRect face = [self modifyFaceRectToSceenBounds:metadataObject.bounds];
            CGRect originalFace = [self modifyFaceRectToSceenBoundsWithoutInsets:metadataObject.bounds];
            
            @try {
                [bowTieImageView setFrame:face]; //FIRST !!!
                
                [Utils fitLayer:bowTieImageView.layer //SECOND !!!
                  inOrientation:[[UIDevice currentDevice] orientation]
        constantVerticalPadding:originalFace.size.height * 1.1f
                verticalPadding:originalFace.size.height * self.draggingPercentage];
                
                bowTieImageView.hidden = NO;
            } @catch (NSException *exception) {
                CLS_LOG(@"EXCEPTION caught: %@", exception);
            }
        }
    }
}

#pragma mark - utils

- (CGFloat)getMaxPaddingThreshold {
    return self.currentMaxThreshold;
}

#pragma mark - bow tie selection delegate

- (void)bowTieSelectionDidSelectBowTie:(BowTie *)bowTie {
    self.currentBowTieImage = bowTie.image;
    self.currentBowTie = bowTie;
    
    for (UIView *bowTieView in [self.cameraView subviews]) {
        if ([bowTieView.layer.name isEqualToString:@"FaceLayer"]) {
            UIImageView *imageView = (UIImageView *)bowTieView;
            imageView.image = self.currentBowTieImage;
        }
    }
}

- (void)bowTieSelectionDidPanBowTie:(CGFloat)panValue {
    self.draggingPercentage = -panValue * kDragThresholdPercentage / self.currentMaxThreshold;
}

- (void)bowTieSelectionDidTapRotateCamera {
    [self hideAllBowTies];
    [self.detector rotateCamera];
}

- (void)bowTieSelectionDidTapTakePicture {
    [[self.cameraView layer] setOpacity:0.0];
    
    __weak FaceDetectionViewController *_weakSelf = self;
    [UIView animateWithDuration:.25 animations:^{
        [[_weakSelf.cameraView layer] setOpacity:1.0];
    }];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [self.detector capturePhotoAsPNGProcessedUpToFilter:self.detectorFilter
                                  withCompletionHandler:^(NSData *processedJPEG, NSError *error) {
                                      UIImage *processedImage = [UIImage imageWithData:processedJPEG];
                                      
                                      if (!self.bowTiesArePresent) {
                                          [self.delegate faceDetectionTookPicture:processedImage];
                                          [MBProgressHUD hideHUDForView:self.view animated:NO];
                                          return;
                                      }
                                      
                                      UIImage *newImage = [self mergeBowTiesWithImage:processedImage];
                                      
                                      [self.delegate faceDetectionTookPicture:newImage];
                                      [MBProgressHUD hideHUDForView:self.view animated:NO];
                                  }];
}

/**
 *	Merges the bowtie images to the currently taken picture
 *
 *	@param bottomImage	UIImage * - the picture that was taken
 *
 *	@return UIImage * - the merged image
 */
- (UIImage *)mergeBowTiesWithImage:(UIImage *)bottomImage {
    UIView *mergingView = [UIView new];
    UIImageView *first = [[UIImageView alloc] initWithImage:bottomImage];
    [mergingView addSubview:first];
    
    CGFloat alpha = bottomImage.size.height / self.cameraView.frame.size.height;
    CGFloat c0 = self.cameraView.frame.size.width * alpha;
    
    for (UIView *view in self.cameraView.subviews) {
        if ([view.layer.name isEqualToString:@"FaceLayer"]) {
            CGRect scaledBowTieRect = CGRectMake(view.frame.origin.x * alpha + (bottomImage.size.width - c0) / 2,
                                                 view.frame.origin.y * alpha,
                                                 view.frame.size.width * alpha,
                                                 view.frame.size.height * alpha);
            view.frame = scaledBowTieRect;
            [mergingView addSubview:view];
        }
    }
    
    
    
    UIGraphicsBeginImageContext(bottomImage.size);
    [mergingView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
