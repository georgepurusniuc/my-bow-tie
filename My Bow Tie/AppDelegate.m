//
//  AppDelegate.m
//  My Bow Tie
//
//  Created by Alex Motoc on 19/01/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import "AppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <Parse/Parse.h>
#import <Realm/Realm.h>

@interface AppDelegate ()
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
     [self migrateRealmDatabase];
    
    // Initialize Parse.
    [Parse enableLocalDatastore];
    [Parse setApplicationId:@"Fm71suChAeKP9oCBjsmtULBpJWwlBm0LZvR35aMi"
                  clientKey:@"bJypXs0FiFQy0B1G8HIp0m13DcLw1vKwWystP1la"];
    
    [self checkForNewBowties]; // get data
    
    [Fabric with:@[CrashlyticsKit]]; //MUST BE LAST LINE
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
}

#pragma mark - Utils

/**
 *	Migrate Realm data if new properties were added or removed
 */
- (void)migrateRealmDatabase {
    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    // Set the new schema version. This must be greater than the previously used
    // version (if you've never set a schema version before, the version is 0).
#warning INCREMENT THIS WHEN CHANGING REALM MODELS
    config.schemaVersion = 2;
    
    // Set the block which will be called automatically when opening a Realm with a
    // schema version lower than the one set above
    config.migrationBlock = ^(RLMMigration *migration, uint64_t oldSchemaVersion) {
        // We haven’t migrated anything yet, so oldSchemaVersion == 0
        if (oldSchemaVersion < 1) {
            // Nothing to do!
            // Realm will automatically detect new properties and removed properties
            // And will update the schema on disk automatically
        }
    };
    
    // Tell Realm to use this new configuration object for the default Realm
    [RLMRealmConfiguration setDefaultConfiguration:config];
    
    // Now that we've told Realm how to handle the schema change, opening the file
    // will automatically perform the migration
    [RLMRealm defaultRealm];
}

// if a new object was added to Parse, download it locally
- (void)checkForNewBowties {
    NSArray *savedIds = [NSArray array];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"bowTiesIds"]) {
        savedIds = [defaults objectForKey:@"bowTiesIds"];
    }
    PFQuery *query = [PFQuery queryWithClassName:@"BowTie"];
    
    [query whereKeyExists:@"objectId"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *quesArray, NSError *error) {
        BOOL shouldDownload = quesArray.count != savedIds.count;
        
        for (PFObject *object in quesArray) {
            if (! [savedIds containsObject:object.objectId]) {
                shouldDownload = YES;
            }
        }
        
        if (shouldDownload) {
            [defaults setObject:@(1) forKey:@"shouldDownloadBowTies"];
            [defaults synchronize];
        }
    }];
    
}

@end
