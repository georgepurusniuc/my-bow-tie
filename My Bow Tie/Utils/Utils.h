//
//  Utils.h
//  My Bow Tie
//
//  Created by Alex Motoc on 19/01/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface Utils : NSObject

+ (void)fitLayer:(CALayer *)layer
   inOrientation:(UIDeviceOrientation)orientation constantVerticalPadding:(CGFloat)constantPadding
 verticalPadding:(CGFloat)padding;

+ (void)showErrorWithMessage:(NSString *)message;
+ (void)showSuccessWithMessage:(NSString *)message;

@end
