//
//  NSString+ContainsString.m
//  My Bow Tie
//
//  Created by Alex Motoc on 17/02/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import "NSString+ContainsString.h"

@implementation NSString (ContainsString)

- (BOOL)stringContainsString:(NSString *)substring {
    if ([self rangeOfString:substring].location == NSNotFound) {
        return NO;
    }
    
    return YES;
}

- (BOOL)containsStringCaseInsensitive:(NSString *)substring {
    return [[self lowercaseString] stringContainsString:[substring lowercaseString]];
}

@end
