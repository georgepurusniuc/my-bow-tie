//
//  X2DetectionCamera+MetadataHandler.m
//
//  Created by X2Mobile on 1/25/14.

#import "X2DetectionCamera+MetadataHandler.h"

@implementation X2DetectionCamera (MetadataHandler)

- (void)    captureOutput:(AVCaptureOutput *)captureOutput
 didOutputMetadataObjects:(NSArray *)metadataObjects
           fromConnection:(AVCaptureConnection *)connection
{
    if (captureOutput == self.faceOutput) {
        [self.detectionDelegate detectorWillOuputFaceMetadata:metadataObjects];
    }
    
    if (captureOutput == self.codeOutput) {
        [self.detectionDelegate detectorWillOuputMachineReadableMetadata:metadataObjects];
    }
    
    if (captureOutput == self.mixedOutput) {
        [self.detectionDelegate detectorWillOuputMachineAndFaceMetadata:metadataObjects];
    }
}


@end
