//
//  X2DetectionCamera+MetadataHandler.h
//
//  Created by X2Mobile on 1/25/14.

#import "X2DetectionCamera_private.h"

@interface X2DetectionCamera (MetadataHandler) <AVCaptureMetadataOutputObjectsDelegate>

@end
