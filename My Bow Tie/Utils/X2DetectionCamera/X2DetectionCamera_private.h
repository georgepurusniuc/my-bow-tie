//
//  X2DetectionCamera_private.h
//
//  Created by X2Mobile on 1/25/14.

#import "GPUImage.h"
#import "X2DetectionCamera.h"

/**
 *  These methods are defined as private because you shouldn't need to modify them for basic detection.
 *  For more advanced detection, feel free to modify these variables and this class as you wish, but I may not be able to help you resolve issues with those changes.
 */

@interface X2DetectionCamera ()

@property (weak) id<X2DetectionDelegate> detectionDelegate;
@property (copy) SMKDetectionBlock detectBlock;

//Properties relating to kFaceFeatures
@property NSArray *coreImageFaceFeatures;
@property CIDetector *faceDetector;
@property CGRect clap;
@property NSInteger idleCount;
@property BOOL processingInProgress;

//Properties relating to kFaceMetadata
@property AVCaptureMetadataOutput *faceOutput;

//Properties relating to kMachineReadableMetaData
@property AVCaptureMetadataOutput *codeOutput;

//Properties relating to kMachineAndFaceMetaData
@property AVCaptureMetadataOutput *mixedOutput;

@end


