//
//  X2DetectionCamera+BlockDelegate.m
//
//  Created by X2Mobile on 1/25/14.

#import "X2DetectionCamera+BlockDelegate.h"
#import "X2DetectionCamera_private.h"

@implementation X2DetectionCamera (BlockDelegate)

- (void)detectorWillOuputFaceFeatures:(NSArray *)faceFeatureObjects inClap:(CGRect)clap
{
    self.detectBlock(kFaceFeatures, faceFeatureObjects, clap);
}

- (void)detectorWillOuputFaceMetadata:(NSArray *)faceMetadataObjects
{
    self.detectBlock(kFaceMetaData, faceMetadataObjects, CGRectZero);
}

- (void)detectorWillOuputMachineReadableMetadata:(NSArray *)machineReadableMetadataObjects
{
    self.detectBlock(kMachineReadableMetaData, machineReadableMetadataObjects, CGRectZero);
}

- (void)detectorWillOuputMachineAndFaceMetadata:(NSArray *)mixedMetadataObjects
{
    self.detectBlock(kMachineAndFaceMetaData, mixedMetadataObjects, CGRectZero);
}

@end
