//
//  UIImage+Utils.h
//  My Bow Tie
//
//  Created by Alex Motoc on 27/07/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Utils)

+ (instancetype)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

@end
