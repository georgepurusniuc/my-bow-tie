//
//  Utils.m
//  My Bow Tie
//
//  Created by Alex Motoc on 19/01/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import "Utils.h"

#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

@implementation Utils

+ (void)fitLayer:(CALayer *)layer
   inOrientation:(UIDeviceOrientation)orientation constantVerticalPadding:(CGFloat)constantPadding
 verticalPadding:(CGFloat)padding {
    CGRect frame = layer.frame;
    
    switch (orientation) {
        case UIDeviceOrientationPortrait:
            [layer setAffineTransform:CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(0.))];
            
            frame.origin.y += constantPadding + padding;
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            [layer setAffineTransform:CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(180.))];
            frame.origin.y += padding;
            frame.origin.y -= constantPadding;
            break;
        case UIDeviceOrientationLandscapeLeft:
            [layer setAffineTransform:CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90))];
            frame.origin.x += padding;
            frame.origin.x -= constantPadding;
            break;
        case UIDeviceOrientationLandscapeRight:
            [layer setAffineTransform:CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(-90))];
            frame.origin.x += padding;
            frame.origin.x += constantPadding;
            break;
        case UIDeviceOrientationFaceUp:
        case UIDeviceOrientationFaceDown:
        default:
            break; // leave the layer in its last known orientation
    }
    
    @try {
        [layer setFrame:frame];
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }
}

+ (void)showErrorWithMessage:(NSString *)message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

+ (void)showSuccessWithMessage:(NSString *)message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

@end
