//
//  NSString+ContainsString.h
//  My Bow Tie
//
//  Created by Alex Motoc on 17/02/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ContainsString)

- (BOOL)stringContainsString:(NSString *)substring;
- (BOOL)containsStringCaseInsensitive:(NSString *)substring;

@end
