//
//  UIView+AnimatedSubviews.h
//  My Bow Tie
//
//  Created by Alex Motoc on 09/02/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^VoidBlock)(void);

@interface UIView (AnimatedSubviews)

- (void)addSubviewAnimated:(UIView *)view completion:(VoidBlock)completion;
- (void)addSubviewAnimated:(UIView *)view;
- (void)removeFromSuperviewAnimated;
- (void)removeFromSuperviewAnimatedWithCompletion:(VoidBlock)completion;

@end
