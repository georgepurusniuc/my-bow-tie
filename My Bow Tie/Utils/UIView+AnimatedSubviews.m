//
//  UIView+AnimatedSubviews.m
//  My Bow Tie
//
//  Created by Alex Motoc on 09/02/15.
//  Copyright (c) 2015 Alex Motoc. All rights reserved.
//

#import "UIView+AnimatedSubviews.h"

@implementation UIView (AnimatedSubviews)

- (void)addSubviewAnimated:(UIView *)view {
    [self addSubviewAnimated:view completion:nil];
}

- (void)addSubviewAnimated:(UIView *)view completion:(VoidBlock)completion {
    [self addSubview:view];
    view.alpha = 0;
    [UIView animateWithDuration:0.3 animations:^{
        view.alpha = 1;
    } completion:^(BOOL finished) {
        if (completion) {
            completion();
        }
    }];
}

- (void)removeFromSuperviewAnimated {
    [self removeFromSuperviewAnimatedWithCompletion:nil];
}

- (void)removeFromSuperviewAnimatedWithCompletion:(VoidBlock)completion {
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        
        if (completion) {
            completion();
        }
    }];
}

@end
